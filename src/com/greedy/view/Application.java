package com.greedy.view;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.greedy.Driver.MemberController;

public class Application {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		MemberController memberController = new MemberController();
		
		do {
			System.out.println("====== 회원 관리 시스템 창 ======");
			System.out.println("1. 회원 전체 조회");
			System.out.println("2. 회원 번호로 조회");
			System.out.println("3. 회원 수정");
			System.out.println("4. 신규 회원 등록");
			System.out.println("5. 회원 탈퇴");
			System.out.println("번호 입력 : ");
			int no = sc.nextInt();
			
			switch(no) {
			case 1: memberController.selectAllMember(); break;
			case 2: memberController.selectOneByNum(inputMemberNum()); break;
			case 3: memberController.modifyMember(inputModifyMember()); break;
			case 4: memberController.registMember(inputMember()); break;
			case 5: memberController.deleteMember(inputMemberNum()); break;
			default : System.out.println("번호를 잘못 입력 했습니다..."); return;
			}
		} while(true);
	}
	private static Map<String, String> inputMemberNum() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("회원 코드 입력 : ");
		String num = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("num", num);
		
		return parameter;
	}
	
private static Map<String, String> inputModifyMember() {
		
		Scanner sc = new Scanner(System.in);
		Map<String, String> parameter = new HashMap<>();
		
		System.out.print("수정할 회원 번호 : ");
		parameter.put("num", sc.nextLine());
		System.out.print("수정할 회원 아이디 : ");
		parameter.put("id", sc.nextLine());
		System.out.print("수정할 회원 비밀번호 : ");
		parameter.put("pw", sc.nextLine());
		System.out.print("수정할 회원 이름 : ");
		parameter.put("name", sc.nextLine());
		System.out.print("수정할 회원 성별 (남/여) : ");
		parameter.put("gender", sc.nextLine());
		System.out.print("수정할 회원 주민등록번호 : ");
		parameter.put("rrn", sc.nextLine());
		System.out.print("수정할 회원 핸드폰 번호 : ");
		parameter.put("phoneNum", sc.nextLine());

		return parameter;
	}
	
	private static Map<String, String> inputMember() {
		
		Scanner sc = new Scanner(System.in);
		Map<String, String> parameter = new HashMap<>();
		
		System.out.print("등록할 회원 아이디 : ");
		parameter.put("id", sc.nextLine());
		System.out.print("등록할 회원 비밀번호 : ");
		parameter.put("pw", sc.nextLine());
		System.out.print("등록할 회원 이름 : ");
		parameter.put("name", sc.nextLine());
		System.out.print("등록할 회원 성별 (남/여) : ");
		parameter.put("gender", sc.nextLine());
		System.out.print("등록할 회원 주민등록번호 : ");
		parameter.put("rrn", sc.nextLine());
		System.out.print("등록할 회원 핸드폰 번호 : ");
		parameter.put("phoneNum", sc.nextLine());
		
		return parameter;
	}
	
	
	
	
	
	
	
	
}
