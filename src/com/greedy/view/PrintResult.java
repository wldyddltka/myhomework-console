package com.greedy.view;

import java.util.List;

import com.greedy.common.MemberDTO;

public class PrintResult {

	public void printMemberList(List<MemberDTO> memberList) {
		for(MemberDTO member : memberList) {
			System.out.println(member);
		}
		
	}

	public void printMember(MemberDTO member) {
		System.out.println(member);
		
	}
	
	public void printErrorMessage(String errorCode) {
		String errorMessage = "";
		switch(errorCode) {
		case "selectList" : errorMessage = "회원 전체 조회에 실패하였습니다"; break;
		case "selectOneByNum" : errorMessage = "회원 조회에 실패하였습니다"; break;
		case "insert" : errorMessage = "신규 회원 등록에 실패하였습니다."; break;
		case "update" : errorMessage = "회원 수정에 실패하였습니다."; break;
		case "delete" : errorMessage = "회원 탈퇴에 실패하였습니다."; break;
		}
		
		System.out.println(errorMessage);
	}

	public void printSuccessMessage(String successCode) {
		String successMessage = "";
		switch(successCode) {
		case "insert" : successMessage = "신규 회원 등록에 성공하였습니다."; break;
		case "update" : successMessage = "회원 수정에 성공하였습니다."; break;
		case "delete" : successMessage = "회원 탈퇴에 성공하였습니다."; break;
		}
		
		System.out.println(successMessage);
		
	}



}
