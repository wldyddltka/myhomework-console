package com.greedy.Driver;

import static com.greedy.common.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.greedy.common.MemberDAO;
import com.greedy.common.MemberDTO;


public class MemberService {

	private final MemberDAO memberDAO ;
	
	public MemberService() {
		memberDAO = new MemberDAO();
	}
	
	/* 회원 전체 조회 */
	public List<MemberDTO> selectAllMember() {
		
		SqlSession sqlSession = getSqlSession();
		
		
		List<MemberDTO> memberList = memberDAO.selectAllMember(sqlSession);
		
		sqlSession.close();
		return memberList;
	}
	/* 회원 번호로 메뉴 조회 */
	public MemberDTO selectOneByNum(int num) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberDTO member = memberDAO.selectOneByNum(sqlSession, num);
		
		sqlSession.close();
		return member;
	}
	/* 회원 수정 */
	public boolean modifyMenu(MemberDTO member) {
		
		SqlSession sqlSession = getSqlSession();
		
		
		int result = memberDAO.updateMember(sqlSession, member);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();
		return result > 0 ? true : false;
	}
	
	/* 신규 회원 등록 */
	public boolean registMember(MemberDTO member) {
		
		SqlSession sqlSession = getSqlSession();
		
		int result = memberDAO.insertMember(sqlSession, member);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();
		return result > 0 ? true : false;
	}
	

	/* 메뉴 삭제 */
	public boolean deleteMember(int num) {
		
		SqlSession sqlSession = getSqlSession();
	
		int result = memberDAO.deleteMember(sqlSession, num);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();
		return result > 0 ? true : false;
	}

	
	
}
