package com.greedy.Driver;

import java.util.List;
import java.util.Map;

import com.greedy.common.MemberDTO;
import com.greedy.view.PrintResult;

public class MemberController {

	private final MemberService memberService;
	private final PrintResult printResult;
	
	public MemberController() {
		memberService = new MemberService();
		printResult = new PrintResult();
	}
	
	/* 화원 전체 조회 메소드 */
	public void selectAllMember() {
		
		List<MemberDTO> memberList = memberService.selectAllMember();
		
		if(memberList != null) {
			printResult.printMemberList(memberList);
		} else {
			printResult.printErrorMessage("selectList");
		}
		
	}
	/* 회원 번호로 조회 메소드 */
	public void selectOneByNum(Map<String, String> parameter) {
		
		int num = Integer.parseInt(parameter.get("num"));
		
		MemberDTO member = memberService.selectOneByNum(num);
		
		if(member != null) {
			printResult.printMember(member);
		} else {
			printResult.printErrorMessage("selectOneByNum");
		}
	}
	/* 회원 수정 메소드 */
	public void modifyMember(Map<String, String> parameter) {
		MemberDTO member = new MemberDTO();
		member.setMemberNo(Integer.parseInt(parameter.get("num")));
		member.setMemberId(parameter.get("id"));
		member.setMemberPw(parameter.get("pw"));
		member.setMemberName(parameter.get("name"));
		member.setMemberGender(parameter.get("gender"));
		member.setMemberRrn(parameter.get("rrn"));
		member.setMemberPhoneNum(parameter.get("phoneNum"));
		
		if(memberService.modifyMenu(member)) {
			printResult.printSuccessMessage("update");
		} else {
			printResult.printErrorMessage("update");
		}	
	}
	
	/* 신규 회원 등록 메소드 */
	public void registMember(Map<String, String> parameter) {
		
		MemberDTO member = new MemberDTO();
		member.setMemberId(parameter.get("id"));
		member.setMemberPw(parameter.get("pw"));
		member.setMemberName(parameter.get("name"));
		member.setMemberGender(parameter.get("gender"));
		member.setMemberRrn(parameter.get("rrn"));
		member.setMemberPhoneNum(parameter.get("phoneNum"));
		
		if(memberService.registMember(member)) {
			printResult.printSuccessMessage("insert");
		} else {
			printResult.printErrorMessage("insert");
		}	
	}

	
	/* 메뉴 삭제 기능 */
	public void deleteMember(Map<String, String> parameter) {
		
		int num = Integer.parseInt(parameter.get("num"));
		
		if(memberService.deleteMember(num)) {
			printResult.printSuccessMessage("delete");
		} else {
			printResult.printErrorMessage("delete");
		}	
		
		
	}

	
	
	
}
