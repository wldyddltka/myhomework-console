package com.greedy.common;

public class MemberDTO {

	private int memberNum;
	private String memberId;
	private String memberPw;
	private String memberName;
	private String memberGender;
	private String memberRrn;
	private String memberPhoneNum;

	public MemberDTO() {}

	public MemberDTO(int memberNum, String memberId, String memberPw, String memberName, String memberGender,
			String memberRrn, String memberPhoneNum) {
		super();
		this.memberNum = memberNum;
		this.memberId = memberId;
		this.memberPw = memberPw;
		this.memberName = memberName;
		this.memberGender = memberGender;
		this.memberRrn = memberRrn;
		this.memberPhoneNum = memberPhoneNum;
	}

	@Override
	public String toString() {
		return "HomeWorkDTO [memberNo=" + memberNum + ", memberId=" + memberId + ", memberPw=" + memberPw
				+ ", memberName=" + memberName + ", memberGender=" + memberGender + ", memberRrn=" + memberRrn
				+ ", memberPhoneNum=" + memberPhoneNum + "]";
	}

	public int getMemberNo() {
		return memberNum;
	}

	public void setMemberNo(int memberNum) {
		this.memberNum = memberNum;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMemberPw() {
		return memberPw;
	}

	public void setMemberPw(String memberPw) {
		this.memberPw = memberPw;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberGender() {
		return memberGender;
	}

	public void setMemberGender(String memberGender) {
		this.memberGender = memberGender;
	}

	public String getMemberRrn() {
		return memberRrn;
	}

	public void setMemberRrn(String memberRrn) {
		this.memberRrn = memberRrn;
	}

	public String getMemberPhoneNum() {
		return memberPhoneNum;
	}

	public void setMemberPhoneNum(String memberPhoneNum) {
		this.memberPhoneNum = memberPhoneNum;
	}

		
	
	
}
