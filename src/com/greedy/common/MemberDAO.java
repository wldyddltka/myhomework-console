package com.greedy.common;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

public class MemberDAO {
	
	/* 회원 전체 조회 */
	public List<MemberDTO> selectAllMember(SqlSession sqlSession) {
		return sqlSession.selectList("MemberMapper.selectAllMember");
	}
	
	/* 회원 번호로 조회 */
	public MemberDTO selectOneByNum(SqlSession sqlSession, int num) {
		return sqlSession.selectOne("MemberMapper.selectOneByNum", num);
	}
	
	/* 회원 수정 */
	public int updateMember(SqlSession sqlSession, MemberDTO member) {
		return sqlSession.update("MemberMapper.updateMember", member);
	}
	
	/* 신규 회원 등록  */
	public int insertMember(SqlSession sqlSession, MemberDTO member) {	
		return sqlSession.insert("MemberMapper.insertMember", member);
	}

	/* 회원 탈퇴 */
	public int deleteMember(SqlSession sqlSession, int num) {
		return sqlSession.delete("MemberMapper.deleteMember", num);
	}


	

	
	
	
	
	
}
